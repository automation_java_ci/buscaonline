## Projeto Automação Java e Gitlab-CI

Exemplo simples de projeto de automação de testes usando Java, Selenium, Cucumber e integração com Gitlab-CI.

## Primeiro passo

Instalação do Java:

```js

$ sudo apt-get install -y software-properties-common

$ sudo add-apt-repository ppa:webupd8team/java -y

$ sudo apt-get update

$ sudo apt-get install oracle-java8-installer

$ java --version

```

## Instalação do Maven (Debian ou Ubuntu)

```js

$ wget -c http://www-us.apache.org/dist/maven/maven-3/3.5.0/source/apache-maven-3.5.0-src.tar.gz

$ cd /home/user_name/Downloads

$ sudo apt install tar

$ tar -zxvf apache-maven-3.5.0-src.tar.gz

$ mv apache-maven-3.5.0-src.tar.gz maven

$ sudo mv maven opt/

$ sudo vim /etc/profile

```
Configuração do arquivo /etc/profile:

```js

export M3_HOME=/opt/maven

export PATH=$M3_HOME/bin:$PATH

mvn -version

```

## Instalação do Maven no Mac



Vejam a documentação : https://maven.apache.org/install.html


## Executando teste local

Após o clone do projeto para rodar os testes dentro do diretório raiz onde temos o arquivo `POM.xml` 

`mvn test -Dcucumber.options="./src/test/java/tests/Feature/realiza_busca.feature"`

ou

`mvn test -Dtest="tests.Feature.RunnerTest"`



