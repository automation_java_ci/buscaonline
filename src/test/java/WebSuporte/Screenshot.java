package WebSuporte;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class Screenshot {
    public static void tirar(WebDriver driverNavigator, String arquivo){
        File screenshot = ((TakesScreenshot)driverNavigator).getScreenshotAs(OutputType.FILE);
        try{
            FileUtils.copyFile(screenshot, new File(arquivo));
        }catch (Exception e){
            System.out.print("Ocorreu erro ao gravar evidencia na pasta" + e.getMessage());
        }
    }
}
