package WebSuporte;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class Generator {

    //data e hora para prints de telas
    public static String dataEHoraParaPrint() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        return new SimpleDateFormat("dd_MM_yyyy HH:mm:ss").format(ts);
    }

}

