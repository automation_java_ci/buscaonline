package WebSuporte;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.concurrent.TimeUnit;


public class WebAccessNavigator {

    public static WebDriver acessarURLBuscador(){
        Dotenv dotenv = Dotenv.configure()
                .directory("./src/test/java/resources")
                .load();

        System.setProperty("webdriver.chrome.driver", dotenv.get("PATH_ENV_DRIVER"));
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--window-size=1364,700","--no-sandbox");

        WebDriver driverNavegacao = new ChromeDriver(options);
        driverNavegacao.manage().window().maximize();
        driverNavegacao.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driverNavegacao.get(dotenv.get("URL_ENVIRONMENT"));
        return driverNavegacao;


    }
}
