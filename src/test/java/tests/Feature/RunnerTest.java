package tests.Feature;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        //Infomar o caminho da feature
        features = "./src/test/java/tests/Feature/realiza_busca.feature",
        tags = {"~@ignore"},
        plugin = {
                //plugin da biblioteca ExtentCucumberFormatter para geração de relatórios
                "com.cucumber.listener.ExtentCucumberFormatter:src/test/java/tests/Reports/fluxo_realiza_busca.html",
                "html:target/cucumber-html-reports"},
        monochrome = true,
        snippets = SnippetType.CAMELCASE,
        dryRun = false,
        strict = false)

public class RunnerTest {
    @AfterClass
    //Configuração de relatórios
    public static void setup() {
        Reporter.loadXMLConfig(new File("./src/test/java/tests/Reports/extent-config.xml"));
        Reporter.setSystemInfo("Thiago Alan", System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Machine", 	"Ubuntu 16.04" + "64 Bit");
        Reporter.setSystemInfo("Selenium", "3.7.0");
        Reporter.setSystemInfo("Maven", "3.3.9");
        Reporter.setSystemInfo("Java Version", "1.8.0_151");
    }


}
