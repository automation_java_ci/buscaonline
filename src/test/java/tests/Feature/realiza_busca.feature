#language: pt

Funcionalidade: Realizar uma busca por um site de busca
    Eu como usuário desejo acessar o buscador
    Para realizar pesquisas

Esquema do Cenário: Acesso ao buscador
    Dado que pesquiso pelo "<informacao>"
    Quando clico no primeiro link da busca que tenha "<resultado>"
    Então valido a "<titulo>" da pagina clicada

  Exemplos:firstHeading
      |informacao               | resultado                               | titulo                        |
      |JUnit – Wikipedia        | JUnit - Wikipedia                       | JUnit                         |
      |Apache Maven - Wikipedia | Apache Maven - Wikipedia                | Apache Maven                  |
      |BDD Cucumber Wiki        | Behavior-driven development - Wikipedia | Behavior-driven development   |


