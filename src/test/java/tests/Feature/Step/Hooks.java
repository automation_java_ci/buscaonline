package tests.Feature.Step;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    @Before(order=1)
    public void beforeScenario(){
        System.out.println("Iniciando o navegador e limpando cookies");
    }
    @Before(order=0)
    public void beforeScenarioStart(){
        System.out.println("-----------------Início do Cenário-----------------");
    }


    @After(order=0)
    public void afterScenarioFinish(){
        System.out.println("-----------------Fim do Cenário-----------------");
    }
    @After(order=1)
    public void afterScenario(){
        System.out.println("Fechando o navegador");
    }

}
