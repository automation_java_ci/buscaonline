package tests.Feature.Step;

import Pages.PageObjectsBuscador;
import WebSuporte.Generator;
import WebSuporte.Screenshot;
import WebSuporte.WebAccessNavigator;
import com.aventstack.extentreports.*;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.WebDriver;

import java.io.UnsupportedEncodingException;

public class realiza_busca_Step {

    //diretorio das imagens "prints" dos testes

    String caminhoEvidencia = "./src/test/Imagens/";
    String ImageDeTeste1  = caminhoEvidencia + "Acessar_site" + Generator.dataEHoraParaPrint() + ".png";
    String ImageDeTeste2  = caminhoEvidencia + "Resultado_busca" + Generator.dataEHoraParaPrint() + ".png";
    String ImageDeTeste3  = caminhoEvidencia + "Mensagem_pagina" + Generator.dataEHoraParaPrint() + ".png";

    //Faz a chamado da classe de pageObjects
    PageObjectsBuscador elementosPages;
    WebDriver driverNavigator;


    @Before
    //Instancia classe que gera reportes dos testes
    public void SetUp() throws UnsupportedEncodingException {
        //Abre url do navegador
        driverNavigator = WebAccessNavigator.acessarURLBuscador();
        ExtentReports extent = new ExtentReports();
        extent.setGherkinDialect("pt");
    }

    @After
    //Fechando navegador após o teste
    public void after() {
        driverNavigator.quit();
    }

    @Dado("^que pesquiso pelo \"([^\"]*)\"$")
    public void quePesquisoPelo(String informacao) throws Throwable {
        elementosPages = new PageObjectsBuscador(driverNavigator);
        Screenshot.tirar(driverNavigator, ImageDeTeste1);
        elementosPages.digitaCampoBusca(informacao);
    }

    @Quando("^clico no primeiro link da busca que tenha \"([^\"]*)\"$")
    public void clicoNoPrimeiroLinkDaBuscaQueTenha(String informacao) throws Throwable {
        Screenshot.tirar(driverNavigator, ImageDeTeste2);
        elementosPages.clicaResultadoDeBusca(informacao);
    }

    @Então("^valido a \"([^\"]*)\" da pagina clicada$")
    public void validoADaPaginaClicada(String titulo) throws Throwable {
        elementosPages.validaTitulo(titulo);
        Screenshot.tirar(driverNavigator, ImageDeTeste3);
    }


}
