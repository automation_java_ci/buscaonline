package Pages;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.TestCase.fail;

public class PageObjectsBuscador extends BaseUrl {

    public PageObjectsBuscador(WebDriver driverNavigator) {

        super(driverNavigator);
    }


    public PageObjectsBuscador digitaCampoBusca(String informacao) {
        //Digita o nome do produto no campo busca
        //Usando o wait webdriver em caso de lentidão na aplicação, ele aguarda o tempo setado em 'timeOutInSeconds' - 40 segundos
        WebDriverWait wait = new WebDriverWait(driverNavigator, 40);
        // 'presenceOfElementLocated' irá aguardar o elemento ficar visível para interagir com ele evitando o Timeout
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']"))).sendKeys(informacao + Keys.ENTER);
        //Clica no botão de busca

        return this;
    }


    public PageObjectsBuscador Scrolldown(int qtdPixel) {
        //Scroll da vertical - barra de rolagem
        //Na variavel qtdPixel informamos quantos pixels será necessário para a barra de rolagem descer.
        JavascriptExecutor js = ((JavascriptExecutor) driverNavigator);
        js.executeScript("window.scrollTo(0,"+ qtdPixel +")");

        return this;
    }

    public PageObjectsBuscador clicaResultadoDeBusca(String informacao) {
        WebDriverWait wait = new WebDriverWait(driverNavigator, 40);
        WebElement linkBuscar = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[contains(text(), '"+ informacao +"')]")));
        linkBuscar.click();
        return this;
    }


    public PageObjectsBuscador validaTitulo(String titulo){
        WebDriverWait wait = new WebDriverWait(driverNavigator, 40);
        //valida mensagem da tela de identificacao do cliente
        WebElement tituloTela = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("firstHeading")));
        Assert.assertEquals(titulo, tituloTela.getText());
        return this;
    }



}
