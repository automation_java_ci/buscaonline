$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./src/test/java/tests/Feature/realiza_busca.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Realizar uma busca por um site de busca",
  "description": "  Eu como usuário desejo acessar o buscador\n  Para realizar pesquisas",
  "id": "realizar-uma-busca-por-um-site-de-busca",
  "keyword": "Funcionalidade"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Acesso ao buscador",
  "description": "",
  "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador",
  "type": "scenario_outline",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 8,
  "name": "que pesquiso pelo \"\u003cinformacao\u003e\"",
  "keyword": "Dado "
});
formatter.step({
  "line": 9,
  "name": "clico no primeiro link da busca que tenha \"\u003cresultado\u003e\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 10,
  "name": "valido a \"\u003ctitulo\u003e\" da pagina clicada",
  "keyword": "Então "
});
formatter.examples({
  "line": 12,
  "name": "firstHeading",
  "description": "",
  "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading",
  "rows": [
    {
      "cells": [
        "informacao",
        "resultado",
        "titulo"
      ],
      "line": 13,
      "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;1"
    },
    {
      "cells": [
        "JUnit – Wikipedia",
        "JUnit - Wikipedia",
        "JUnit"
      ],
      "line": 14,
      "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;2"
    },
    {
      "cells": [
        "Apache Maven - Wikipedia",
        "Apache Maven - Wikipedia",
        "Apache Maven"
      ],
      "line": 15,
      "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;3"
    },
    {
      "cells": [
        "BDD Cucumber Wiki",
        "Behavior-driven development - Wikipedia",
        "Behavior-driven development"
      ],
      "line": 16,
      "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;4"
    }
  ],
  "keyword": "Exemplos"
});
formatter.before({
  "duration": 442723,
  "status": "passed"
});
formatter.before({
  "duration": 280546,
  "status": "passed"
});
formatter.before({
  "duration": 2222704946,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Acesso ao buscador",
  "description": "",
  "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;2",
  "type": "scenario",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 8,
  "name": "que pesquiso pelo \"JUnit – Wikipedia\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Dado "
});
formatter.step({
  "line": 9,
  "name": "clico no primeiro link da busca que tenha \"JUnit - Wikipedia\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 10,
  "name": "valido a \"JUnit\" da pagina clicada",
  "matchedColumns": [
    2
  ],
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "JUnit – Wikipedia",
      "offset": 19
    }
  ],
  "location": "realiza_busca_Step.quePesquisoPelo(String)"
});
formatter.result({
  "duration": 1318681259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "JUnit - Wikipedia",
      "offset": 43
    }
  ],
  "location": "realiza_busca_Step.clicoNoPrimeiroLinkDaBuscaQueTenha(String)"
});
formatter.result({
  "duration": 1758416616,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "JUnit",
      "offset": 10
    }
  ],
  "location": "realiza_busca_Step.validoADaPaginaClicada(String)"
});
formatter.result({
  "duration": 157686354,
  "status": "passed"
});
formatter.after({
  "duration": 75549678,
  "status": "passed"
});
formatter.after({
  "duration": 106544,
  "status": "passed"
});
formatter.after({
  "duration": 90585,
  "status": "passed"
});
formatter.before({
  "duration": 105747,
  "status": "passed"
});
formatter.before({
  "duration": 81811,
  "status": "passed"
});
formatter.before({
  "duration": 1312664894,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Acesso ao buscador",
  "description": "",
  "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;3",
  "type": "scenario",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 8,
  "name": "que pesquiso pelo \"Apache Maven - Wikipedia\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Dado "
});
formatter.step({
  "line": 9,
  "name": "clico no primeiro link da busca que tenha \"Apache Maven - Wikipedia\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 10,
  "name": "valido a \"Apache Maven\" da pagina clicada",
  "matchedColumns": [
    2
  ],
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "Apache Maven - Wikipedia",
      "offset": 19
    }
  ],
  "location": "realiza_busca_Step.quePesquisoPelo(String)"
});
formatter.result({
  "duration": 964968216,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Apache Maven - Wikipedia",
      "offset": 43
    }
  ],
  "location": "realiza_busca_Step.clicoNoPrimeiroLinkDaBuscaQueTenha(String)"
});
formatter.result({
  "duration": 1584729157,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Apache Maven",
      "offset": 10
    }
  ],
  "location": "realiza_busca_Step.validoADaPaginaClicada(String)"
});
formatter.result({
  "duration": 257398408,
  "status": "passed"
});
formatter.after({
  "duration": 66365117,
  "status": "passed"
});
formatter.after({
  "duration": 133103,
  "status": "passed"
});
formatter.after({
  "duration": 146679,
  "status": "passed"
});
formatter.before({
  "duration": 659261,
  "status": "passed"
});
formatter.before({
  "duration": 806181,
  "status": "passed"
});
formatter.before({
  "duration": 1301101950,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Acesso ao buscador",
  "description": "",
  "id": "realizar-uma-busca-por-um-site-de-busca;acesso-ao-buscador;firstheading;4",
  "type": "scenario",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 8,
  "name": "que pesquiso pelo \"BDD Cucumber Wiki\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Dado "
});
formatter.step({
  "line": 9,
  "name": "clico no primeiro link da busca que tenha \"Behavior-driven development - Wikipedia\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 10,
  "name": "valido a \"Behavior-driven development\" da pagina clicada",
  "matchedColumns": [
    2
  ],
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "BDD Cucumber Wiki",
      "offset": 19
    }
  ],
  "location": "realiza_busca_Step.quePesquisoPelo(String)"
});
formatter.result({
  "duration": 1009955974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Behavior-driven development - Wikipedia",
      "offset": 43
    }
  ],
  "location": "realiza_busca_Step.clicoNoPrimeiroLinkDaBuscaQueTenha(String)"
});
formatter.result({
  "duration": 1862504468,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Behavior-driven development",
      "offset": 10
    }
  ],
  "location": "realiza_busca_Step.validoADaPaginaClicada(String)"
});
formatter.result({
  "duration": 232811480,
  "status": "passed"
});
formatter.after({
  "duration": 65877273,
  "status": "passed"
});
formatter.after({
  "duration": 45934,
  "status": "passed"
});
formatter.after({
  "duration": 33467,
  "status": "passed"
});
});